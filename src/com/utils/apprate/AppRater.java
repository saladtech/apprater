package com.utils.apprate;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class AppRater {
	private String APP_TITLE;
	private String APP_PNAME;

	private String btnRateText = "Rate Now ";
	private String btnLaterText = "Rate later";

	public AppRater(String title, String pkname) {
		APP_TITLE = title;
		APP_PNAME = pkname;
		btnRateText = "Rate - " + title;
	}


	public void showRateDialog(final Context mContext,
		final SharedPreferences.Editor editor) {
		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);

		FrameLayout llBack = new FrameLayout(mContext);
		llBack.setBackgroundColor(Color.parseColor("#000000"));

		LinearLayout ll = new LinearLayout(mContext);

		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setBackgroundColor(Color.parseColor("#F0F0F0"));
		ll.setPadding(5, 5, 5, 5);
		ll.setLayoutParams(lp);

		TextView tv = new TextView(mContext);
		tv.setText("If you enjoy using " + APP_TITLE
				+ ", please take a moment to rate it. Thanks for your support!");

		tv.setLayoutParams(lp);
		tv.setPadding(4, 5, 4, 0);
		ll.addView(tv);

		Button b1 = new Button(mContext);
		b1.setText(btnRateText);
		b1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri
						.parse("market://details?id=" + APP_PNAME)));
				dialog.dismiss();
			}
		});
		b1.setLayoutParams(lp);
		b1.setPadding(0, 5, 0, 0);
		b1.setGravity(Gravity.CENTER);
		ll.addView(b1);

		Button b2 = new Button(mContext);
		b2.setText(btnLaterText);
		b2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		b2.setLayoutParams(lp);
		b2.setPadding(0, 5, 0, 0);
		b2.setGravity(Gravity.CENTER);
		ll.addView(b2);

		llBack.addView(ll);

		dialog.setContentView(llBack);
		dialog.show();
	}
}
